//
//  BillSplitsApp.swift
//  BillSplits
//
//  Created by Franz Henri De Guzman on 6/14/23.
//

import SwiftUI

@main
struct BillSplitsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
