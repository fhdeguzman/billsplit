//
//  ContentView.swift
//  BillSplits
//
//  Created by Franz Henri De Guzman on 6/14/23.
//

import SwiftUI

struct ContentView: View {
    @State private var billAmount = ""
    @State private var numberOfPeople = "1"
    @State private var splitAmount: Double = 0.0
    
    func calculateSplitAmount() {
        guard let bill = Double(billAmount), let people = Int(numberOfPeople), people > 0 else {
            splitAmount = 0.0
            return
        }
        splitAmount = bill / Double(people)
    }
    
    var body: some View {
        LinearGradient(
            gradient: Gradient(colors: [Color(hex: "#F8C8DC"), Color(hex: "#63666A")]),
            startPoint: .top,
            endPoint: .bottom
        )

        .edgesIgnoringSafeArea(.all)
        .overlay(
            VStack(spacing: 20) {
                TextField("Bill Amount", text: $billAmount)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .multilineTextAlignment(.center)
                    .padding()
                    .onChange(of: billAmount) { newValue in
                        calculateSplitAmount()
                    }
                
                HStack {
                    Button(action: {
                        if let amount = Int(numberOfPeople), amount > 0 {
                            numberOfPeople = String(amount - 1)
                            calculateSplitAmount()
                        }
                    }) {
                        Image(systemName: "minus.circle")
                    }
                    
                    TextField("Number of People", text: $numberOfPeople)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .multilineTextAlignment(.center)
                        .foregroundColor(.black)
                        .padding()
                    
                    Button(action: {
                        if let amount = Int(numberOfPeople) {
                            numberOfPeople = String(amount + 1)
                            calculateSplitAmount()
                        }
                    }) {
                        Image(systemName: "plus.circle")
                    }
                }
                .font(.title)
                .foregroundColor(.white)
                .padding(.horizontal)
                
                if splitAmount > 0 {
                    ScrollView {
                        LazyVGrid(columns: Array(repeating: GridItem(.flexible(), spacing: 10), count: 2), spacing: 10) {
                            ForEach(0..<Int(numberOfPeople)!, id: \.self) { index in
                                ZStack {
                                    RoundedRectangle(cornerRadius: 10)
                                        .foregroundColor(Color.pink)
                                        .shadow(color: .black, radius: 3, x: 0, y: 2)
                                    
                                    Text("\(splitAmount, specifier: "%.2f") PHP")
                                        .font(.title)
                                        .foregroundColor(.white)
                                }
                                .frame(height: 100)
                            }
                        }
                        .padding(.horizontal)
                    }
                }
                
                Spacer()
            }
        )
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


extension Color {
    init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 1
        var rgbValue: UInt64 = 0
        scanner.scanHexInt64(&rgbValue)

        let r = Double((rgbValue & 0xFF0000) >> 16) / 255.0
        let g = Double((rgbValue & 0x00FF00) >> 8) / 255.0
        let b = Double(rgbValue & 0x0000FF) / 255.0

        self.init(red: r, green: g, blue: b)
    }
}
